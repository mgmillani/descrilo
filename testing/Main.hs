module Main where

import Data.DescriLo

main = do
	putStrLn "Showing:"
	cats <- loadDescriptionFile "config.cat" "none"
	-- mapM_ print (filter (\x -> (length $ values x) > 3) cats)
	mapM_ print cats
	
	putStrLn "Reading:"
	let scats = concat $ map show cats
	let rcats = loadDescriptionString scats "none"
	-- mapM_ print (filter (\x -> (length $ values x) > 3) rcats)
	mapM_ print cats
	
	putStrLn "Check Attribute:"
	let test x = (name x) ++ 
		if checkAttribute "a" (\x -> x == "b") x then
			": a == b"
		else
			": a != b"
	
	mapM_ putStrLn $ map test cats